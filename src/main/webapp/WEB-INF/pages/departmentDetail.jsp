<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:template>
    <jsp:attribute name="header">
        <h1>${department.name} <small>${department.location.city}, ${department.location.province}, ${department.location.country.countryName}</small></h1>
    </jsp:attribute>
  <jsp:body>
    <c:url var="url" value="/departments"/>
    <a href="${url}" class="btn btn-primary"><span class="glyphicon glyphicon-arrow-left"></span> Back</a>

    <div class="panel panel-primary">
      <div class="panel-heading">Employees</div>
      <div class="panel-body">
        <table class="table table-hover" style="display: ${department.employees.size() > 0 ? 'block' : 'none'}">
          <thead>
          <tr>
            <th>Id</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Job title</th>
          </tr>
          </thead>
          <tbody>
          <c:forEach items="${department.employees}" var="employee">
            <c:url var="url" value="/employees">
              <c:param name="id" value="${employee.employeeId}"/>
            </c:url>
            <tr class="clickable-row" data-url="${url}">
              <td>${employee.employeeId}</td>
              <td>${employee.firstname}</td>
              <td>${employee.lastname}</td>
              <td>${employee.job.jobTitle}</td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
    </div>
  </jsp:body>
</t:template>