<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:template>
    <jsp:attribute name="header">
        <h1>${employee.firstname} ${employee.lastname} <small>${employee.job.jobTitle}</small></h1>
    </jsp:attribute>
    <jsp:body>
        <c:url var="url" value="/employees"/>
        <a href="${url}" class="btn btn-primary"><span class="glyphicon glyphicon-arrow-left"></span> Back</a>

        <div class="panel panel-primary">
            <div class="panel-heading">Personal details</div>
            <div class="panel-body">
                <h5>Id: </h5>
                <span>${employee.employeeId}</span>
                <h5>Email: </h5>
                <span>${employee.email}</span>
                <h5>Phone number: </h5>
                <span>${employee.phoneNumber}</span>
                <h5>Hiredate: </h5>
                <span>${employee.hireDate}</span>
                <h5>Salary: </h5>
                <span>${employee.salary}</span>
                <h5>Commission: </h5>
                <span>${employee.commission}</span>
                <h5>Manager: </h5>
                <span>${employee.manager.firstname} ${employee.manager.lastname}</span>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">Department</div>
            <div class="panel-body">
                    ${employee.department.name}
            </div>
        </div>
    </jsp:body>
</t:template>