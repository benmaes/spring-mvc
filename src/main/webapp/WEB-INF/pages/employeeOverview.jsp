<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:template>
    <jsp:attribute name="header">
        <h1>Employees</h1>
    </jsp:attribute>
    <jsp:body>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Job title</th>
                    <th>Department</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${employees}" var="employee">
                <c:url var="url" value="/employees">
                    <c:param name="id" value="${employee.employeeId}"/>
                </c:url>
                <tr class="clickable-row" data-url="${url}">
                    <td>${employee.employeeId}</td>
                    <td>${employee.firstname}</td>
                    <td>${employee.lastname}</td>
                    <td>${employee.job.jobTitle}</td>
                    <td>${employee.department.name}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </jsp:body>
</t:template>
