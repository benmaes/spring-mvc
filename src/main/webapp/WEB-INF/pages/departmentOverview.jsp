<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:template>
    <jsp:attribute name="header">
        <h1>Departments</h1>
    </jsp:attribute>
  <jsp:body>
    <table class="table table-hover">
      <thead>
      <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Location</th>
        <th>Manager</th>
        <th>Number of employees</th>
      </tr>
      </thead>
      <tbody>
      <c:forEach items="${departments}" var="department">
        <c:url var="url" value="/departments">
          <c:param name="id" value="${department.departmentId}"/>
        </c:url>
        <tr class="clickable-row" data-url="${url}">
          <td>${department.departmentId}</td>
          <td>${department.name}</td>
          <td>${department.location.city}, ${department.location.province}, ${department.location.country.countryName}</td>
          <td>${department.manager.firstname} ${department.manager.lastname}</td>
          <td class="text-right">${department.employees.size()}</td>
        </tr>
      </c:forEach>
      </tbody>
    </table>
  </jsp:body>
</t:template>
