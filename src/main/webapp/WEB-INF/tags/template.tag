<%@tag description="Overall Page Template" pageEncoding="UTF-8" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="url" required="false" type="java.lang.String" %>
<html>
<head>
    <title>Home Project</title>
    <link rel="stylesheet" href="styles/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="styles/css/personal.css"/>
</head>
<body>
<nav class="navbar navbar-default navbar-static-top navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Spring mvc</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/employees">Employees</a></li>
                <li><a href="/departments">Departments</a></li>
                <li><a href="/about">About</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div id="pageheader" class="col-md-12 jumbotron">
        <jsp:invoke fragment="header"/>
    </div>
</div>
<div id="body" class="container">
    <jsp:doBody/>
</div>
<div class="container">
    <div id="pagefooter" class="col-md-12 footer">
        <p>Copyright 2015, Ben</p>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="styles/js/bootstrap.min.js"></script>
<script src="styles/js/personal.js"></script>
</body>
</html>
