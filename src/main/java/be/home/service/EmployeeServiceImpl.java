package be.home.service;

import be.home.entity.Employee;
import be.home.resource.EmployeeResource;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeResource employeeResource;

    public EmployeeServiceImpl(EmployeeResource employeeResource) {
        this.employeeResource = employeeResource;
    }

    @Override
    public List<Employee> getEmployees() {
        return employeeResource.getEmployees();
    }

    @Override
    public List<Employee> getEmployeesByDepartmentId(int id) {
        return employeeResource.getEmployeesByDepartmentId(id);
    }

    @Override
    public Employee getEmployeeById(int id) {
        return employeeResource.getEmployeeById(id);
    }
}
