package be.home.service;

import be.home.entity.Department;
import be.home.resource.DepartmentResource;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class DepartmentServiceImpl implements DepartmentService {

    private DepartmentResource departmentResource;
    private EmployeeService employeeService;

    public DepartmentServiceImpl(DepartmentResource departmentResource) {
        this.departmentResource = departmentResource;
    }

    @Override
    public List<Department> getDepartments() {
        List<Department> departments = departmentResource.getDepartments();

        for(Department department : departments) {
            department.setEmployees(employeeService.getEmployeesByDepartmentId(department.getDepartmentId()));
        }

        return departments;
    }

    @Override
    public Department getDepartmentById(int id) {
        return departmentResource.getDepartmentById(id);
    }

    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }
}
