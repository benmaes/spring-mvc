package be.home.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

@Entity
@Table(name = "departments")
public class Department {
    @Id
    @GeneratedValue
    @Column(name = "department_id")
    private int departmentId;
    @Column(name = "department_name")
    private String name;
    @ManyToOne
    @JoinColumn(name="manager_id")
    private Employee manager;
    @ManyToOne
    @JoinColumn(name="location_id")
    private Location location;
    @Transient
    private List<Employee> employees;

    public Department() {

    }

    public Department(int id, String name) {
        this.departmentId = id;
        this.name = name;
    }

    public Department(int id) {
        this.departmentId = id;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee getManager() {
        return manager;
    }

    public void setManager(Employee manager) {
        this.manager = manager;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
        return name + "(" + location + ")";
    }
}
