package be.home.controller;

import be.home.service.DepartmentService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@RequestMapping("/departments")
public class DepartmentController {
    private DepartmentService departmentService;

    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getDepartments() {
        return new ModelAndView("departmentOverview", "departments", departmentService.getDepartments());
    }

    @RequestMapping(method = RequestMethod.GET, params = "id")
    public ModelAndView getDepartment(@RequestParam int id) {
        return new ModelAndView("departmentDetail", "department", departmentService.getDepartmentById(id));
    }
}
