package be.home.controller;

import be.home.service.EmployeeService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@RequestMapping("/employees")
public class EmployeeController {
    private EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getEmployees() {
        return new ModelAndView("employeeOverview", "employees", employeeService.getEmployees());
    }

    @RequestMapping(method = RequestMethod.GET, params = "id")
    public ModelAndView getEmployeeDetails(@RequestParam int id) {
        return new ModelAndView("employeeDetail", "employee", employeeService.getEmployeeById(id));
    }
}
