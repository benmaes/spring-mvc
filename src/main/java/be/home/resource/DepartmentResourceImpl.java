package be.home.resource;

import be.home.entity.Department;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.util.List;

public class DepartmentResourceImpl implements DepartmentResource {

    private EntityManager entityManager;

    public DepartmentResourceImpl(EntityManagerFactory entityManagerFactory) {
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public List<Department> getDepartments() {
        Query query = entityManager.createQuery("SELECT d FROM Department d");
        return query.getResultList();
    }


    @Override
    public Department getDepartmentById(int id) {
        Query query = entityManager.createQuery("SELECT d FROM Department d WHERE d.departmentId = :id");
        query.setParameter("id", id);
        return (Department) query.getSingleResult();
    }
}
