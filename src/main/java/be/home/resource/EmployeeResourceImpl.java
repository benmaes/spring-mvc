package be.home.resource;

import be.home.entity.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.util.List;

public class EmployeeResourceImpl implements EmployeeResource {

    private EntityManager entityManager;

    public EmployeeResourceImpl(EntityManagerFactory entityManagerFactory) {
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public List<Employee> getEmployees() {
        Query query = entityManager.createQuery("SELECT e FROM Employee e");
        return query.getResultList();
    }

    @Override
    public List<Employee> getEmployeesByDepartmentId(int id) {
        Query query = entityManager.createQuery("SELECT e FROM Employee e WHERE e.department.departmentId = :id");
        query.setParameter("id", id);
        return query.getResultList();
    }

    @Override
    public Employee getEmployeeById(int id) {
        Query query = entityManager.createQuery("SELECT e FROM Employee e WHERE e.employeeId = :id");
        query.setParameter("id", id);
        return (Employee) query.getSingleResult();
    }
}
