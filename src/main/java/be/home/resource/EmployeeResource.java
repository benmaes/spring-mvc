package be.home.resource;

import be.home.entity.Employee;

import java.util.List;

public interface EmployeeResource {
    List<Employee> getEmployees();
    List<Employee> getEmployeesByDepartmentId(int id);
    Employee getEmployeeById(int id);
}
