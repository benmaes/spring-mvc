package be.home.resource;

import be.home.entity.Department;

import java.util.List;

public interface DepartmentResource {
    List<Department> getDepartments();
    Department getDepartmentById(int id);
}
